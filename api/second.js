import ajax from '@/common/ajax.js'
const {
	http
} = uni.$u
import conf from '@/common/config.js';

export const upload_api = (params) => http.upload(conf.url_config + '/api/common/upload', {
	filePath: params.filePath
})

//检查合伙人资格
export const qualification_check_api = () => {
	return ajax.get({
		url: '/api/partner/partner/checkQualification',
		toast: false,
	})
}

//申请成为合伙人
export const apply_api = (data) => {
	return ajax.post({
		url: '/api/partner/partner/apply',
		data
	})
}

//成为合伙人
export const becomePartner_api = () => {
	return ajax.post({
		url: '/api/partner/partner/becomePartner',
	})
}

//得到合伙人信息
export const partner_baseinfo_api = () => {
	return ajax.get({
		url: '/api/partner/Partner/baseinfo',
	})
}

//消息公告
export const getNotice_api = (params) => {
	return ajax.get({
		url: '/api/partner/notice/getNotice',
		params
	})
}

//获取协议列表
export const agreementlist_api = (params) => {
	return ajax.get({
		url: '/api/partner/partner/agreementlist',
		params
	})
}

//保存协议签字
export const save_agreement_fileFile_api = (data) => {
	return ajax.post({
		url: '/api/partner/partner/fileFile',
		data
	})
}
//提交实名信息
export const submitRealInfo_api = (data) => {
	return ajax.post({
		url: '/api/partner/partner/submitRealInfo',
		data
	})
}
//等级提升信息
export const levelInfo_api = () => {
	return ajax.get({
		url: '/api/partner/partner/levelInfo',
	})
}
//等级提升信息
export const doLevelUpgrade_api = (data) => {
	return ajax.post({
		url: '/api/partner/partner/doLevelUpgrade',
		data
	})
}

//我的团队等级消息
export const partner_levels_api = (params) => {
	return ajax.get({
		url: '/api/partner/partner/levels',
		params
	})
}
//我的团队列表
export const partner_myteam_list_api = (params) => {
	return ajax.get({
		url: '/api/partner/partner/myteam',
		params
	})
}
//团队成员详情
export const myteam_memberInfo_api = (params) => {
	return ajax.get({
		url: '/api/partner/partner/memberInfo',
		params
	})
}
//合伙人申请列表
export const waitingApplyList_api = (params) => {
	return ajax.get({
		url: '/api/partner/partner/waitingApplyList',
		params
	})
}

//直接赋予合伙人资格
export const assignrole_api = (data) => {
	return ajax.post({
		url: '/api/partner/partner/assignrole',
		data
	})
}
//流水信息
export const stoneLog_api = (params) => {
	return ajax.get({
		url: '/api/partner/partner/stoneLog',
		toast: false,
		params
	})
}

//石头转账
export const stoneTransfer_api = (data) => {
	return ajax.post({
		url: '/api/partner/partner/stoneTransfer',
		data
	})
}
//处理合伙人申请
export const dealapply_api = (data) => {
	return ajax.post({
		url: '/api/partner/partner/dealapply',
		data
	})
}

//生成合伙人二维码
export const qrcode_api = (data) => {
	return ajax.get({
		url: '/api/partner/partner/qrcode',
		data
	})
}

//我的团队等级消息
export const check_file_is_signed = (params) => {
	return ajax.get({
		url: '/api/partner/partner/checkFileIssigned',
		params
	})
}


export const check_has_vip_qualification = (params) => {
	return ajax.get({
		url: '/api/partner/partner/checkHasVipQualifiction',
		params
	})
}