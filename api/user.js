import ajax from '@/common/ajax.js'

export const sendLogin = function (data) {
    return ajax.post({
        url: '/api/user/sendLogin',
        data: data
    })
}

export const sendProfile = function (data) {
    return ajax.post({
        url: '/api/user/sendProfile',
        data: data
    })
}

export const getRawUserInfo = function (data) {
    return ajax.get({
        url: '/api/user/getRawUserInfo',
        data: data
    })
}


export const zeroOrderPay_api = function (data,activity_id) {
    return ajax.post({
        url: `/api/order/zeroOrderPay?activity_id=${activity_id}`,
        data: data
    })
}

export const getUserAddress = function (data) {
    return ajax.post({
        url: '/api/user/getUserAddress',
        data: data
    })
}

export const sendUserAddress = function (data) {
    return ajax.post({
        url: '/api/user/sendUserAddress',
        data: data
    })
}

export const sendTake = function (data) {
    return ajax.post({
        url: '/api/order/sendTake',
        data: data
    })
}

export default {
	sendLogin,
    sendProfile,
    getUserAddress,
    sendUserAddress,
	getRawUserInfo,
	zeroOrderPay_api,
	sendTake
}