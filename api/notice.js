import ajax from '@/common/ajax.js'

export const getNiticeOne = function (data) {
    return ajax.post({
        url: '/api/notice/getOne',
        data: data
    })
}

export const getNoticeInfo = function (data) {
    return ajax.post({
        url: '/api/notice/getInfo',
        data: data
    })
}

export default {
    getNiticeOne,
    getNoticeInfo
}