import ajax from '@/common/ajax.js'
import user from './user'
import activity from './activity'
import notice from './notice'
import merchant from './merchant'
import transaction from './transaction'

export const commonConfig = function (data) {
    return ajax.post({
        url: '/api/common/config',
        data: data
    })
}

// 短信
export const sendCode = function (data) {
    return ajax.post({
        url: '/api/user/sendCode',
        data: data
    })
}

// 焦点图
export const getBanner = function (data) {
    return ajax.post({
        url: '/api/common/getBanner',
        data: data
    })
}

// 焦点图详情
export const getBannerInfo = function (data) {
    return ajax.post({
        url: '/api/common/getBannerInfo',
        data: data
    })
}

export const sendReport = function (data) {
    return ajax.post({
        url: '/api/report/sendReport',
        data: data
    })
}

export const sendAccountissue = function (data) {
    return ajax.post({
        url: '/api/accountissue/sendAccountissue',
        data: data
    })
}

export default {
	commonConfig,
    sendCode,
    getBanner,
    getBannerInfo,
    sendReport,
    sendAccountissue,
    ...user,
    ...activity,
    ...notice,
    ...merchant,
	...transaction,
}