import ajax from '@/common/ajax.js'

export const getTransactionInfo = function (data) {
    return ajax.get({
        url: '/api/order/getTransactionInfo',
        params: data
    })
}

export const createTransaction = function (data) {
    return ajax.get({
        url: '/api/order/createTransaction',
        params: data
    })
}

export const jssdkConfig = function (data) {
    return ajax.post({
        url: '/api/common/jssdkConfig',
        data: data
    })
}


export default {
	getTransactionInfo,
	createTransaction,
	jssdkConfig
}