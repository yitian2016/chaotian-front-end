import ajax from '@/common/ajax.js'

export const templateDetailNew = function(data) {
	return ajax.get({
		url: '/api/activity/templateDetail',
		params: data
	})
}

export const getActivity = function(data) {
	return ajax.post({
		url: '/api/activity/getActivity',
		data: data
	})
}

export const getActivityNew = function(data) {
	return ajax.get({
		url: '/api/activity/getActivity',
		params: data
	})
}

export const getActivityList = function(data) {
	return ajax.post({
		url: '/api/activity/getActivityList',
		data: data
	})
}

export const getShopNameList = function(data) {
	return ajax.post({
		url: '/api/activity/getShopNameList',
		data: data
	})
}

export const setStatus = function(data) {
	return ajax.post({
		url: '/api/activity/setStatus',
		data: data
	})
}

export const setDel = function(data) {
	return ajax.post({
		url: '/api/activity/setDel',
		data: data
	})
}

export const templateList = function(data) {
	return ajax.post({
		url: '/api/activity/templateList',
		data: data
	})
}

export const templateDetail = function(data) {
	return ajax.post({
		url: '/api/activity/templateDetail',
		data: data
	})
}

export const createTemp = function(data) {
	return ajax.post({
		url: '/api/activity/createTemp',
		data: data
	});
}


export const sendTemp = function(data) {
	return ajax.post({
		url: '/api/activity/sendTemp',
		data: data
	});
}

export const createQrcode = function(data) {
	return ajax.post({
		url: '/api/activity/createQrcode',
		data: data
	});
}

export const orderSend = function(data) {
	return ajax.post({
		url: '/api/order/send',
		data: data
	});
}

export const getUserActivity = function(data) {
	return ajax.post({
		url: '/api/order/getUserList',
		data: data
	})
}

export const sendOrderTake = function(data) {
	return ajax.post({
		url: '/api/order/sendTake',
		data: data
	});
}

export const getShopActivity = function(data) {
	return ajax.post({
		url: '/api/order/getShopList',
		data: data
	});
}

export const getShopOrder = function(data) {
	return ajax.post({
		url: '/api/order/getShopInfo',
		data: data
	});
}

export const getUserInfo = function(data) {
	return ajax.post({
		url: '/api/order/getUserInfo',
		data: data
	});
}
export const getOrderByOrderNo = function(data) {
	return ajax.get({
		url: '/api/order/getOrderByOrderNo',
		params: data
	});
}

export const activitySendOut = function(data) {
	return ajax.post({
		url: '/api/order/sendOut',
		data: data
	});
}

export const activitySendOutIn = function(data) {
	return ajax.post({
		url: '/api/order/sendOutIn',
		data: data
	});
}


export const saveActivity = function(data, id) {
	return ajax.post({
		url: id ? '/api/activity/saveActivity?id=' + id : '/api/activity/saveActivity',
		data: data
	});
}


//生成草稿
export const saveDraft = function(data, id) {
	return ajax.post({
		url: id ? '/api/activity/saveDraft?id=' + id : '/api/activity/saveDraft',
		data: data
	});
}

export default {
	getActivity,
	getActivityList,
	getShopNameList,

	setStatus,
	setDel,

	templateList,
	templateDetail,
	sendTemp,
	createQrcode,

	orderSend,
	getUserActivity,
	sendOrderTake,
	getShopActivity,
	getShopOrder,
	getUserInfo,
	getOrderByOrderNo,
	activitySendOut,
	activitySendOutIn,
	templateDetailNew,
	createTemp,
	saveActivity,
	saveDraft,
	getActivityNew,
}