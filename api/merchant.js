import ajax from '@/common/ajax.js'

export const getMerchantCount = function (data) {
    return ajax.post({
        url: '/api/merchant/getCount',
        data: data
    })
}

export const sendMerchantRequest = function (data) {
    return ajax.post({
        url: '/api/merchant/sendRequest',
        data: data
    })
}

export const getRequestList = function (data) {
    return ajax.post({
        url: '/api/merchant/getRequestList',
        data: data
    })
}

// 续费
export const sendPayhistory = function (data) {
    return ajax.post({
        url: '/api/payhistory/sendPayhistory',
        data: data
    })
}

export default {
    getMerchantCount,
    sendMerchantRequest,
    getRequestList,
    sendPayhistory,
}