import Vue from 'vue'
import App from './App'

Vue.config.productionTip = false
App.mpType = 'app'

import ajax from './api'
Vue.prototype.$API = ajax
// import VConsole from 'vconsole';

// const vConsole = new VConsole();
// // 或者使用配置参数来初始化，详情见文档


// // 结束调试后，可移除掉
// vConsole.destroy();

// 引入全局uView
import uView from 'uview-ui';
Vue.use(uView);

import store from '@/store';

// 引入uView提供的对vuex的简写法文件
let vuexStore = require('@/store/$u.mixin.js');
Vue.mixin(vuexStore);

let customMixin = require('@/common/custom.mixin.js');
Vue.mixin(customMixin);

const app = new Vue({
    store,
    ...App
})
app.$mount()
