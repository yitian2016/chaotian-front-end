import ajax from 'uni-ajax'
import store from '@/store'
import conf from './config.js'

// 创建请求实例
const instance = ajax.create({
	// 初始配置
	baseURL: conf.url_config
})

// 添加请求拦截器
instance.interceptors.request.use(
	config => {
        var token = uni.getStorageSync('lifeData')?.vuex_token;
        
		// 在发送请求前做些什么
		if (token) {
			// 让每个请求携带自定义token 请根据实际情况自行修改
			config.header['Token'] = token
		}
		return config
	},
	error => {
		// 对请求错误做些什么
		return Promise.reject(error)
	}
)

// 添加响应拦截器
instance.interceptors.response.use(
	res => {
		// 对响应数据做些什么
		const data = res.data
		const code = res.data.code
		if (code < 1) {
			if(res.config?.toast === false){
				return Promise.reject(res)
			}
            uni.showToast({
                title: res.data.message || res.data.msg,
                icon: 'none'
            });

			return Promise.reject(res)
		} else {
            if (code === 401) {
                uni.showModal({
                    title: '温馨提示',
                    content: '请先登录，才能进行下面操作！',
                    success: function (res) {
						store.commit('$uStore', {name: 'vuex_token', value: ''})
						store.commit('$uStore', {name: 'vuex_user', value: {}})
						
                        if (res.confirm) {
                            uni.navigateTo({
                                url: '/pages/my/login'
                            })
                        } else if (res.cancel) {
                            uni.navigateBack()
                        }
                    }
                })
            } else {
                return data
            }
		}
	},
	error => {
		// 对响应错误做些什么
		return Promise.reject(error)
	}
)

// 导出 create 创建后的实例
export default instance
