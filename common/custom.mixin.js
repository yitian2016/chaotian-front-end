
import {
	check_has_vip_qualification
} from '@/api/second.js'

// #ifdef H5
var jweixin = require('jweixin-module')
// #endif

module.exports = {
	data() {
		return {
			shareTitle: '超田分享系统',
			shareDesc: '欢迎来到超田分享系统',
			share1v1Image: 'https://h5.pyhhzapp.com/uploads/20240206/82028989edd96f29d8f6a84ba141b791.png',
		}
	},
	onShow() {
	
		// #ifdef H5
		this.share()
		// #endif
	
	},
	methods: {
		goApplyPartnerPage() {
			if (this.vuex_token == '') {
				uni.navigateTo({
					url: '/pages/my/login'
				})
				return false;
			}
			check_has_vip_qualification().then(res => {
				if (res.data.has_qualification) {
					return uni.navigateTo({
						url: '/pages/my/index'
					})
				} else {
					uni.navigateTo({
						url: '/pages/second/partner'
					})
				}
			})
		},
		
		// #ifdef H5
		share() {
			let url = window.location.href
			let that = this
			this.$API.jssdkConfig({
				url: url,
				api_list: ['updateAppMessageShareData', 'updateTimelineShareData'].join("-")
			}).then(res => {
				// console.log(res);
				let data = res.data
				if (res.code) {
					jweixin.config({
						debug: false,
						appId: data.appId,
						timestamp: data.timestamp,
						nonceStr: data.nonceStr,
						signature: data.signature,
						jsApiList: data.jsApiList,
					});
					jweixin.ready(function() {
						var shareData = {
							title: that.shareTitle,
							desc: that.shareDesc,
							link: url,
							imgUrl: that.share1v1Image,
							success: function(res) {
								console.log(res);
							},
							cancel: function(res) {}
						};
		
						//分享给朋友接口  
						jweixin.updateAppMessageShareData(shareData);
		
						//分享到朋友圈接口  
						jweixin.updateTimelineShareData(shareData);
					})
					jweixin.error(function(res) {
						// config信息验证失败会执行error函数，如签名过期导致验证失败，具体错误信息可以打开config的debug模式查看，也可以在返回的res参数中查看，对于SPA可以在这里更新签名。
						console.log('anything wrong');
						console.log(res);
					})
				}
			})
		
		},
		// #endif
	}
}