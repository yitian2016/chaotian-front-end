export default {
	getUrlParam(path, name) {
		var reg = new RegExp("(^|\\?|&)" + name + "=([^&]*)(\\s|&|$)", "i")

		if (reg.test(path)) {
			return unescape(RegExp.$2.replace(/\+/g, " "))
		}

		return ""
	},

	isWechatBrowser() {
		// #ifdef H5
		let ua = navigator.userAgent.toLowerCase()
		if (ua.match(/MicroMessenger/i) == "micromessenger") {
			return true
		}
		return false
		// #endif
		// #ifndef H5
		return false
		// #endif
	},

	isNormalBrowser() {
		// #ifdef H5
		let ua = navigator.userAgent.toLowerCase()
		if (ua.match(/MicroMessenger/i) != "micromessenger") {
			return true
		}
		return false
		// #endif
		// #ifndef H5
		return false
		// #endif
	},

	getMapDistance(lng1, lat1, lng2, lat2) {
		var radLat1 = lat1 * Math.PI / 180.0;
		var radLat2 = lat2 * Math.PI / 180.0;
		var a = radLat1 - radLat2;
		var b = lng1 * Math.PI / 180.0 - lng2 * Math.PI / 180.0;
		var s = 2 * Math.asin(Math.sqrt(Math.pow(Math.sin(a / 2), 2) +
			Math.cos(radLat1) * Math.cos(radLat2) * Math.pow(Math.sin(b / 2), 2)));
		s = s * 6378.137; // EARTH_RADIUS;
		s = Math.round(s * 10000) / 10000;

		s = s * 1000

		if (isNaN(s)) {
			return 0;
		}
		s = Math.floor(s / 1000 * 100) / 100;
		return s;
	},
	
	formatDate(ts) {
		const date = new Date(ts);
		
		// 获取各部分
		const year = date.getFullYear();
		const month = (date.getMonth() + 1).toString().padStart(2, '0'); // 月份从0开始，所以需要+1
		const day = date.getDate().toString().padStart(2, '0');
		const hours = date.getHours().toString().padStart(2, '0');
		const minutes = date.getMinutes().toString().padStart(2, '0');
		const seconds = date.getSeconds().toString().padStart(2, '0');
		
		// 格式化为 yyyy-mm-dd hh:mm:ss
		const formattedDate = `${year}-${month}-${day} ${hours}:${minutes}`;
		return formattedDate
	}
}
